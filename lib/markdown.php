<?php
function markdown($buf){
	$ispre = 0;
	$isul = 0;
	$isol = 0;
	$isp = 0;
	$trows = 0;
	$ul=array("*", "-", "+");
	$h=array("######", "#####", "####", "###", "##", "#");
	$pre=array("```", "\t");
	$blockquote=array(">");
	$hrs=array("---", "\*\*\*");

	foreach(preg_split("/((\r?\n)|(\r\n?))/", $buf) as $l){
		// pre tags
		foreach($pre as $tok){
			if($ispre == 0){
				if(preg_match("/(^\\" . $tok . ")/", $l)){
					$ispre = 1;
					$l = "<pre><code class='" . substr($l, 3) . "'>";
					printf("%s", $l);
					continue 2;	// exit if-else and go to next loop
				}
			} else if($ispre == 1){
				if(preg_match("/(^\\" . $tok . ")/", $l)){
					$ispre = 0;
					$l = "</code></pre" . substr($l, 3) . ">";
					continue 1;
				}
			}
		}

		if($ispre){	// exit early 
			printf("%s\n", str_replace("\```", "```", htmlentities($l)));
			continue 1;
		}

		// tables
		if(preg_match("/^\| /", $l)){
			$row = array_values(array_filter(explode("|", $l)));
			$table[$trows] = $row;
			$trows++;
		}

		if($trows > 0){	
			if(preg_match("/^\s*$/", $l)){
				// re-define trows because of array filtering
				$table = array_filter($table);
				$trows = count($table);
				$cols = count($table[0]);

				// get alignment
				for($i=0; $i<$cols; $i++){
					if(preg_match("/:-+-:/", $table[1][$i])){
						$al = "center"; 
					} else if(preg_match("/:-+-/", $table[1][$i])){
						$al = "left"; 
					} else if(preg_match("/-+-:/", $table[1][$i])){
						$al = "right"; 
					} else {
						$al = ""; 
					}

					$alignment[$i] = $al;
				}

				printf("<table>\n");
				for($i=0; $i<$trows; $i++){
					if($i!=1)
						printf("<tr>");
					for($j=0; $j<$cols; $j++){
						if($i == 0){
							printf("<th align='%s'>%s</th>", $alignment[$j], $table[$i][$j]);
						} else if($i == 1){
							continue 2;
						} else {
							printf("<td align='%s'>%s</td>", $alignment[$j], $table[$i][$j]);
						}
					}
					printf("</tr>\n");
				}

				printf("</table>\n");
				$trows = 0;
			} else {
				continue 1;
			}
		}

		// unordered lists
		foreach($ul as $tok){
			if(preg_match("/(^\s*\\" . $tok . " )/", $l)){
				$l = preg_replace("/^\s+/", "", $l);
				$l = "<li>" . substr($l, 2) . "</li>";
				$isul = $isul + 1;
			} else if($isul >= 1 && preg_match("/^\s*$/", $l)) {
				printf("</ul>\n");
				$isul = 0;
			}
		}

		// ordered lists
		if(preg_match("/^\s*[0-9]*\. /", $l)){
			$l = "<li>" . substr($l, 2) . "</li>";
			$isol = $isol + 1;
		} else if($isol >= 1 && preg_match("/^\s*$/", $l)) {
			printf("</ol>\n");
			$isol = 0;
		}

		// blockquotes
		foreach($blockquote as $tok){
			if(preg_match("/(^\\" . $tok . " )/", $l)){
				$l = "<blockquote>" . substr($l, 2) . "</blockquote>";
			}
		}

		// headers
		foreach($h as $tok){
			if(preg_match("/(^\\" . $tok . " )/", $l)){
				$len=strlen($tok);
				$l = "<h" . $len . ">" . substr($l, $len) . "</h" . $len . ">";
			}
		}

		// catch images
		if(preg_match_all('/!?\!\[([^\]]*)\]\(([^\)]+)\)/', $l, $mpos, PREG_OFFSET_CAPTURE)){
			for($i=0; $i<count($mpos[0]); $i++){
				$htmlizedurl = "<img src='" . $mpos[2][$i][0] . "' alt='" . $mpos[1][$i][0]  . "'>";
				$l = str_replace($mpos[0][$i][0], $htmlizedurl, $l);
			}

		}

		// catch links
		if(preg_match_all('/!?\[([^\]]*)\]\(([^\)]+)\)/', $l, $mpos, PREG_OFFSET_CAPTURE)){
			for($i=0; $i<count($mpos[0]); $i++){
				$htmlizedurl = "<a href='" . $mpos[2][$i][0] . "'>" . $mpos[1][$i][0]  . "</a>";
				$l = str_replace($mpos[0][$i][0], $htmlizedurl, $l);
			}
		}

		// hr els
		foreach($hrs as $tok){
			if(preg_match("/(^\\" . $tok . "*$)/", $l)){
				$l = "<hr>";
			}
		}

		// inline emphasizers
		$l = preg_replace("/(^(\*\*|__)|( \*\*| __))/", " <b>", $l);
		$l = preg_replace("/((\*\*|__)$|(\*\*|__)+)/", "</b>", $l);
		$l = preg_replace("/(^(\*|_)|( \*| _))/", " <i>", $l);
		$l = preg_replace("/((\*|_)$|(\*|_)+)/", "</i>", $l);
		$l = preg_replace("/ \`/", " <code>", $l);
		$l = preg_replace("/\`/", "</code> ", $l);

		// printer 
		if($isol == 1){	// list handler
			printf("<ol>\n%s\n", $l);
			continue 1;
		} else if($isul == 1){
			printf("<ul>\n%s\n", $l);
			continue 1;
		}
		
		if(preg_match("/^\s*$/", $l) && !$isp){ // paragraph handler
			printf("<p>\n");
			$isp = 1;
		} else if(preg_match("/^\s*$/", $l) && $isp) {
			printf("</p>\n");
			$isp = 0;
		} else {
			printf("%s\n", $l);
		}
	}

}
?>
