<?php
session_start();
include "htmlmodules.php";

$uid = $_SESSION['uid'];
$name = $_SESSION['user'];
$loggedin = $_SESSION['loggedin'];
$license = $_SESSION['license'];

$err = 0;

# grab user info (necessary)
mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
$db = new mysqli($sqlhost, $sqluser, $sqlpass, $sqldb);
$st = $db->prepare("select uid from users where uid=?");
$st->bind_param('i', $uid);
$st->execute();
$res = $st->get_result();
$row = $res->fetch_assoc();

# check license
$licenses = array("copyright", "copyleft", "cc-zero", "cc-by", "cc-by-sa", "cc-by-nc", "cc-by-nc-sa", "cc-by-nd", "cc-by-nc-nd");

foreach($licenses as $l){
	if(!strcmp($_POST['license'], $l)){
		$licenseexists = 1;
		$license = $l;
	}
}

if($licenseexists != 1)
	$err = 3;

# check that the current user owns this post and is logged in
if(!isset($loggedin))
	$err = 1;
       
if($loggedin != true)
	$err = 1;

if($row['uid'] != $_SESSION['uid'])
	$err = 2;


switch($err){
	case 0:
		continue;
	case 1:
		html_header("Not logged in");
		html_body("You are not logged in. Please refer to <a href='login.php'> to log in or wait to be redirected");
		header("refresh:3;url=/lib/login.php");
		die();
	case 2:
		html_header("Cannot set theme");
		html_body("You cannot set a theme for a different user ( ͡° ͜ʖ ͡°) ");
		die();
	case 3:
		html_header("license is not supported");
		html_body("<img style='display: inline-block; width: 20%;' src='/assets/images/shodan.png'><blockquote style='display: inline-block; width: 60%;'>Look at you, hacker: a pathetic creature of meat and bone, panting and sweating as you run through my corridors. How can you challenge a perfect, immortal machine?</blockquote>");
		die();
}


# insert theme
$db = new mysqli($sqlhost, $sqluser, $sqlpass, $sqldb);
$st = $db->prepare("update users set license=? where uid=?");
$st->bind_param('si', strip_tags($license), $_SESSION['uid']);
$st->execute();
$db->close();

$_SESSION['license'] = $license;

html_header("Default license updated!");
html_body("Default license updated!");
html_footer("");
header("refresh:1;url=/lib/mypage.php");
die();
?>
