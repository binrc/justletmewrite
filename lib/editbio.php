<?php

session_start();
include "htmlmodules.php";

$uid = $_SESSION['uid'];
$name = $_SESSION['user'];
$loggedin = $_SESSION['loggedin'];

$err = 0;

# grab post info (necessary)
mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
$db = new mysqli($sqlhost, $sqluser, $sqlpass, $sqldb);
$st = $db->prepare("select bio,uid from users where uid=?");
$st->bind_param('i', $uid);
$st->execute();
$res = $st->get_result();
if($res){
	$row = $res->fetch_assoc();
} else {
	echo "sql broke" . $db->errno;
	die();
}
$db->close();

# check that the current user owns this post and is logged in
if(!isset($loggedin))
	$err = 1;
       
if($loggedin != true)
	$err = 1;

if($row['uid'] != $_SESSION['uid'])
	$err = 2;


switch($err){
	case 0:
		continue;
	case 1:
		html_header("Not logged in");
		html_body("You are not logged in. Please refer to <a href='login.php'> to log in or wait to be redirected");
		header("refresh:3;url=/lib/login.php");
		die();
	case 2:
		html_header("Cannot edit bio");
		html_body("You cannot edit a bio that you do not own ( ͡° ͜ʖ ͡°) ");
		die();
}

# generate form
html_header($row['title'] . " (edit)");

printf("<h3>Bio (edit)</h3>
<form action='do_editbio.php' method='post' id='updatebio'>
<label for='bio'>Edit bio: <br></label>
<textarea name='bio' form='updatebio' id='bio' rows='12' cols='40' wrap='soft' required>%s</textarea><br>
<button type='submit'>Save</button></form>", $row['bio']);

html_footer("");
?>
