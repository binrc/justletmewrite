<?php
session_start();
include "htmlmodules.php";

$uid = $_SESSION['uid'];
$name = $_SESSION['user'];
$loggedin = $_SESSION['loggedin'];

$err = 0;

# grab user info (necessary)
mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
$db = new mysqli($sqlhost, $sqluser, $sqlpass, $sqldb);
$st = $db->prepare("select uid from users where uid=?");
$st->bind_param('i', $uid);
$st->execute();
$res = $st->get_result();
$row = $res->fetch_assoc();
$db->close();

# check for duplicates
$hash = hash('sha256', strip_tags($_POST['post']));
$db = new mysqli($sqlhost, $sqluser, $sqlpass, $sqldb);
$st = $db->prepare("select voter,post from ratings where post=? and voter=?;");
$st->bind_param('ii', $_GET['pid'], $_SESSION['uid']);
$st->execute();
$res = $st->get_result();
// if hash exists
if(mysqli_num_rows($res) != 0){
	$err = 3;
}

$db->close();

# check that the current user owns this post and is logged in
if(!isset($loggedin))
	$err = 1;
       
if($loggedin != true)
	$err = 1;

if($row['uid'] != $_SESSION['uid'])
	$err = 2;


switch($err){
	case 0:
		continue;
	case 1:
		html_header("Not logged in");
		html_body("You are not logged in. Please refer to <a href='login.php'> to log in or wait to be redirected");
		header("refresh:3;url=/lib/login.php");
		die();
		break;
	case 2:
		html_header("Cannot comment");
		html_body("You cannot comment as someone else ( ͡° ͜ʖ ͡°) ");
		die();
		break;
	case 3:
		html_header("You have already voted");
		html_body("You cannot vote twice or submit a modified grade");
		die();
		break;
}


# insert vote
$db = new mysqli($sqlhost, $sqluser, $sqlpass, $sqldb);
$st = $db->prepare("insert into ratings(post,voter,rating) values(?,?,?);");
$st->bind_param('iii', $_GET['pid'], $_SESSION['uid'], $_POST['rating']);
$st->execute();
$db->close;

html_header("Vote added!");
html_body("Vote added!");
html_footer("");
header("refresh:1;url=/lib/post.php?pid=" . $_GET['pid']);
die();
?>
