<?php
session_start();
include "htmlmodules.php";

$uid = $_SESSION['uid'];
$name = $_SESSION['user'];
$loggedin = $_SESSION['loggedin'];
$pid = $_GET['pid'];

$err = 0;

# grab post info (necessary)
mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
$db = new mysqli($sqlhost, $sqluser, $sqlpass, $sqldb);
$st = $db->prepare("select author,content,title from posts where pid=?");
$st->bind_param('i', $pid);
$st->execute();
$res = $st->get_result();
if($res){
	$row = $res->fetch_assoc();
} else {
	echo "sql broke" . $db->errno;
	die();
}
$db->close();

# check that the current user owns this post and is logged in
if(!isset($loggedin))
	$err = 1;
       
if($loggedin != true)
	$err = 1;

if($row['author'] != $_SESSION['uid'])
	$err = 2;


switch($err){
	case 0:
		continue;
	case 1:
		html_header("Not logged in");
		html_body("You are not logged in. Please refer to <a href='login.php'> to log in or wait to be redirected");
		header("refresh:3;url=/lib/login.php");
		die();
	case 2:
		html_header("Cannot delete post");
		html_body("You cannot delete a post that you did not write ( ͡° ͜ʖ ͡°) ");
		die();
}

# generate form
html_header($row['title'] . " (delete)");
printf("<h1>%s (delete)</h1>
<pre>%s</pre>
<form action='do_deletepost.php?pid=%s' method='post' id='deletepost'>
<label for='check'>Check this box if you're sure you want to delete this post</label>
<input type='checkbox' name='check' id='check' required><br>
<button type='submit'>Delete</button></form>", $row['title'], $row['content'], $pid);

html_footer("");
?>
