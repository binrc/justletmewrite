<?php
session_start();
include "htmlmodules.php";

$err = 0;

if(array_key_exists('uid', $_SESSION)){
	$uid = $_SESSION['uid'];
} else {
	$err = 1;
}

if(array_key_exists('user', $_SESSION))
	$name = $_SESSION['user'];

if(array_key_exists('loggedin', $_SESSION))
	$loggedin = $_SESSION['loggedin'];

if(strlen(strip_tags($_POST['comment'])) <= 500)
	$err = 4;

# grab user info (necessary)
mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
$db = new mysqli($sqlhost, $sqluser, $sqlpass, $sqldb);
$st = $db->prepare("select uid from users where uid=?");
$st->bind_param('i', $uid);
$st->execute();
$res = $st->get_result();
$row = $res->fetch_array();

# check hashes
$hash = hash('sha256', strip_tags($_POST['comment']));
$db = new mysqli($sqlhost, $sqluser, $sqlpass, $sqldb);
$st = $db->prepare("select comment from commenthashes where chash=?;");
$st->bind_param('s', $hash);
$st->execute();
$res = $st->get_result();

// if hash exists
if(mysqli_num_rows($res) != 0){
	$err = 3;
}

$db->close();

# check that the current user owns this post and is logged in
if(!isset($loggedin))
	$err = 1;
       
if($loggedin != true)
	$err = 1;

if($row['uid'] != $_SESSION['uid'])
	$err = 2;


switch($err){
	case 0:
		continue;
	case 1:
		html_header("Not logged in");
		html_body("You are not logged in. Please refer to <a href='login.php'> to log in or wait to be redirected");
		header("refresh:3;url=/lib/login.php");
		die();
		break;
	case 2:
		html_header("Cannot comment");
		html_body("You cannot comment as someone else ( ͡° ͜ʖ ͡°) ");
		die();
		break;
	case 3:
		html_header("Comment already exists");
		html_body("Your comment must be unique and original");
		printf("<h3>Add comment</h3>
		<form action='do_addcomment.php?pid=%s' method='post' id='addcomment'>
		<label for='comment'>Say something: <br></label>
		<textarea name='comment' form='addcomment' id='comment' rows='12' cols='40' wrap='soft' required minlength='500'>%s</textarea><br>
		<button type='submit'>Save</button></form>", $_GET['pid'], $_POST['comment']);
		die();
		break;
	case 4:
		html_header("Comment is too short");
		html_body("Your comment must be at least 500 characters long. ");
		printf("<h3>Add comment</h3>
		<form action='do_addcomment.php?pid=%s' method='post' id='addcomment'>
		<label for='comment'>Say something: <br></label>
		<textarea minlength='500' name='comment' form='addcomment' id='comment' rows='12' cols='40' wrap='soft' required>%s</textarea><br>
		<button type='submit'>Save</button></form>", $_GET['pid'], $_POST['comment']);
		
		die();
		break;
}


# insert post
$db = new mysqli($sqlhost, $sqluser, $sqlpass, $sqldb);
$st = $db->prepare("insert into comments(post,author,content) values(?,?,?);");
$st->bind_param('iis', $_GET['pid'], $_SESSION['uid'], strip_tags($_POST['comment']) );
$st->execute();
$db->close;

# get cid 
$db = new mysqli($sqlhost, $sqluser, $sqlpass, $sqldb);
$st = $db->prepare("select cid from comments where content=?;");
$st->bind_param('s', strip_tags($_POST['comment']));
$res = $st->execute();
$res = $st->get_result();
if($res)
	$row = mysqli_fetch_array($res);

$db->close();

# insert hashes
$db = new mysqli($sqlhost, $sqluser, $sqlpass, $sqldb);
$st = $db->prepare("insert into commenthashes(comment, chash) values(?,?);");
$st->bind_param("is", $row['cid'], $hash);
$st->execute();
$db->close();

html_header("Comment added!");
html_body("Comment added!");
html_footer("");
header("refresh:1;url=/lib/post.php?pid=" . $_GET['pid']);
die();
?>
