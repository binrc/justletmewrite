<?php
session_start();
include "htmlmodules.php";

$err = 0;

if(array_key_exists('uid', $_SESSION)){
	$uid = $_SESSION['uid'];
} else {
	$err = 1;
}

if(array_key_exists('user', $_SESSION))
	$name = $_SESSION['user'];

if(array_key_exists('loggedin', $_SESSION))
	$loggedin = $_SESSION['loggedin'];

if(strlen(strip_tags($_POST['post'])) <= 2000)
	$err = 3;


if(!isset($loggedin))
	$err = 1;
       
if($loggedin != true)
	$err = 1;

# check license
$licenses = array("copyright", "copyleft", "cc-zero", "cc-by", "cc-by-sa", "cc-by-nc", "cc-by-nc-sa", "cc-by-nd", "cc-by-nc-nd");

foreach($licenses as $l){
	if(!strcmp($_POST['license'], $l)){
		$licenseexists = 1;
		$license = $l;
	}
}

if($licenseexists != 1)
	$err = 4;


# check hashes
$hash = hash('sha256', strip_tags($_POST['post']));
$db = new mysqli($sqlhost, $sqluser, $sqlpass, $sqldb);
$st = $db->prepare("select post from posthashes where phash=?");
$st->bind_param('s', $hash);
$st->execute();
$res = $st->get_result();
// if hash exists
if(mysqli_num_rows($res) != 0){
	$err = 2;
}

$db->close();

switch($err){
	case 0:
		continue;
	case 1:
		html_header("Not logged in");
		html_body("You are not logged in. Please refer to <a href='login.php'> to log in or wait to be redirected");
		header("refresh:3;url=/lib/login.php");
		die();
		break;
	case 2:
		html_header("This post already exists");
		html_body("This post is not original.");
		# post creator
		
		printf("<h3>Fix your post</h3><form action='do_newpost.php' method='post' id='newpost'><label for='title'>Title: </label><input type='text' name='title' id='title' required autocomplete='off' value='%s'><br><label for='post'>Write something: <br></label><textarea minlength='2000' name='post' form='newpost' id='post' rows='24' cols='40' wrap='soft' required>%s</textarea><br>
<label for='license'>Choose a license: </label>
<select name='license' id='license'>
<option value='%s'>Default: %s</option>
<option value='copyright'>copyright</option>
<option value='copyleft'>copyleft</option>
<option value='cc-zero'>cc-zero</option>
<option value='cc-by'>cc-by</option>
<option value='cc-by-sa'>cc-by-sa</option>
<option value='cc-by-nc'>cc-by-nc</option>
<option value='cc-by-nc-sa'>cc-by-nc-sa</option>
<option value='cc-by-nd'>cc-by-nd</option>
<option value='cc-by-nc-nd'>cc-by-nc-nd</option><button type='submit'>Save Post</button></form>", $_POST['title'], $_POST['post'], $_POST['license'], $_POST['license']);
		html_footer("");
		die();
		break;
	case 3:
		html_header("This post is not long enough");
		html_body("Your post is not long enough. All posts must be 2000 characters or more. ");
		# post creator
		
		printf("<h3>Fix your post</h3><form action='do_newpost.php' method='post' id='newpost'><label for='title'>Title: </label><input type='text' name='title' id='title' required autocomplete='off' value='%s'><br><label for='post'>Write something: <br></label><textarea minlength='2000' name='post' form='newpost' id='post' rows='24' cols='40' wrap='soft' required>%s</textarea><br>
<label for='license'>Choose a license: </label>
<select name='license' id='license'>
<option value='%s'>Default: %s</option>
<option value='copyright'>copyright</option>
<option value='copyleft'>copyleft</option>
<option value='cc-zero'>cc-zero</option>
<option value='cc-by'>cc-by</option>
<option value='cc-by-sa'>cc-by-sa</option>
<option value='cc-by-nc'>cc-by-nc</option>
<option value='cc-by-nc-sa'>cc-by-nc-sa</option>
<option value='cc-by-nd'>cc-by-nd</option>
<option value='cc-by-nc-nd'>cc-by-nc-nd</option>
</select><button type='submit'>Save Post</button></form>", $_POST['title'], $_POST['post'], $_POST['license'], $_POST['license']);
		html_footer("");
		die();
		break;
	case 4:
		html_header("license is not supported");
		html_body("<img style='display: inline-block; width: 20%;' src='/assets/images/shodan.png'><blockquote style='display: inline-block; width: 60%;'>Look at you, hacker: a pathetic creature of meat and bone, panting and sweating as you run through my corridors. How can you challenge a perfect, immortal machine?</blockquote>");
		die();
}

# grab user info 
/*
mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
$db = new mysqli($sqlhost, $sqluser, $sqlpass, $sqldb);

$st = $db->prepare("select uid,name,bio from users where uid=?");

$st->bind_param('i', $uid);

$st->execute();
$res = $st->get_result();
$row = $res->fetch_assoc();
$db->close();
 */


# insert post
$db = new mysqli($sqlhost, $sqluser, $sqlpass, $sqldb);
$st = $db->prepare("insert into posts(author, content, title, license) values(?,?,?,?);");
$st->bind_param('isss', $_SESSION['uid'], strip_tags($_POST['post']), strip_tags($_POST['title']), strip_tags($license));
$res = $st->execute();
$db->close();

# get post id
$db = new mysqli($sqlhost, $sqluser, $sqlpass, $sqldb);
$st = $db->prepare("select pid from posts where content=?;");
$st->bind_param('s', strip_tags($_POST['post']));
$res = $st->execute();
$res = $st->get_result();
if($res)
	$row = mysqli_fetch_array($res);

	
$db->close();

# insert hashes
$db = new mysqli($sqlhost, $sqluser, $sqlpass, $sqldb);
$st = $db->prepare("insert into posthashes(post, phash) values(?,?);");
$st->bind_param("is", $row['pid'], $hash);
$st->execute();
$db->close();


html_header("Post added!");
html_body("Post added!");
html_footer("");
header("refresh:1;url=/lib/mypage.php");
die();
?>
