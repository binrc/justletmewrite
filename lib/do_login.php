<?php
session_start();
include "htmlmodules.php";
html_header("Submit");
// initialize with no errors
$err = 0;

// check not null
if($_POST['username'] != NULL)
	$u = $_POST['username'];
else $err=1;

if($_POST['password'] != NULL)
	$p = $_POST['password'];
else $err=1;

// get passhas from db
mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
$db = new mysqli($sqlhost, $sqluser, $sqlpass, $sqldb);
$st = $db->prepare("select license,name,passhash,uid from users where name = ?");
$st->bind_param('s', $u);
$st->execute();
$res = $st->get_result();
$row = $res->fetch_assoc();
$db->close();

// compare hashes
$p = $p . $salt; // append salt
$p = hash('sha512', $p);

if($row['passhash'] != $p)
	$err = 2;

// handle errors
switch($err){
case 0:
	continue;
case 1: 
	die("Error: Username/password cannot be blank\n");
	break;
case 2:
	die("Error: Username/password is incorrect\n");
	break;
}


// create unique session 
session_destroy();
session_start();
$_SESSION['loggedin'] = true;
$_SESSION['user'] = $row['name'];
$_SESSION['uid'] = $row['uid'];
$_SESSION['license'] = $row['license'];
session_regenerate_id(true);

// display some bs
printf("<h1>%s has logged in</h1>", $u);
printf("<p>Wait to be redirected or go to the <a href='/index.php'>index</a> manually</p>");
html_footer("");
header("refresh:3;url=/index.php");
die();
?>
