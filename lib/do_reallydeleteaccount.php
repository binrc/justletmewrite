<?php
session_start();
include "htmlmodules.php";
html_header("Submit");
// initialize with no errors
$err = 0;

// check not null
if($_POST['username'] != NULL)
	$u = $_POST['username'];
else $err=1;

if($_POST['password'] != NULL)
	$p = $_POST['password'];
else $err=1;

if(!isset($_POST['reallydelete']))
	$err = 3;

// get passhas from db
mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
$db = new mysqli($sqlhost, $sqluser, $sqlpass, $sqldb);
$st = $db->prepare("select license,name,passhash,uid from users where name = ?");
$st->bind_param('s', $u);
$st->execute();
$res = $st->get_result();
$row = $res->fetch_assoc();
$db->close();

// compare hashes
$p = $p . $salt; // append salt
$p = hash('sha512', $p);

if($row['passhash'] != $p)
	$err = 2;

// handle errors
switch($err){
case 0:
	continue;
case 1: 
	die("Error: Username/password cannot be blank\n");
	break;
case 2:
	die("Error: Username/password is incorrect\n");
	break;
case 3:
	die("Error: you did not check the checkbox\n");
	break;
}


// create unique session 
session_destroy();
session_start();
$_SESSION['loggedin'] = true;
$_SESSION['user'] = $row['name'];
$_SESSION['uid'] = $row['uid'];
$_SESSION['license'] = $row['license'];
session_regenerate_id(true);

// delete comments 
printf("<h1>Deleting comments</h1>", $u);

# grab comments
mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
$db = new mysqli($sqlhost, $sqluser, $sqlpass, $sqldb);
$st = $db->prepare("select cid from comments where author=?");

$st->bind_param('i', $_SESSION['uid']);
$st->execute();
$res = $st->get_result();

// set up second and third connections
$db2 = new mysqli($sqlhost, $sqluser, $sqlpass, $sqldb);
$st2 = $db2->prepare("delete from commenthashes where comment=?");

$db3 = new mysqli($sqlhost, $sqluser, $sqlpass, $sqldb);
$st3 = $db3->prepare("delete from comments where cid=?");

// iterate on results
while($row = mysqli_fetch_assoc($res)){
	$st2->bind_param('i', $row['cid']);
	$st2->execute();
	$st3->bind_param('i', $row['cid']);
	$st3->execute();
}

$db ->close();
$db2 ->close();
$db3 ->close();

// delete posts 
printf("<h1>Deleting posts</h1>", $u);

# grab posts
mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
$db = new mysqli($sqlhost, $sqluser, $sqlpass, $sqldb);
$st = $db->prepare("select pid from posts where author=?");
$st->bind_param('i', $_SESSION['uid']);
$st->execute();
$res = $st->get_result();

// set up second, third, fourth, and fifth connections
$db2 = new mysqli($sqlhost, $sqluser, $sqlpass, $sqldb);
$st2 = $db2->prepare("select * from comments where post=?");

$db3 = new mysqli($sqlhost, $sqluser, $sqlpass, $sqldb);
$st3 = $db3->prepare("delete from commenthashes where comment=?");

$db4 = new mysqli($sqlhost, $sqluser, $sqlpass, $sqldb);
$st4 = $db4->prepare("delete from comments where post=?");

$db5 = new mysqli($sqlhost, $sqluser, $sqlpass, $sqldb);
$st5 = $db5->prepare("delete from posthashes where post=?");

$db6 = new mysqli($sqlhost, $sqluser, $sqlpass, $sqldb);
$st6 = $db6->prepare("delete from ratings where post=?");

$db7 = new mysqli($sqlhost, $sqluser, $sqlpass, $sqldb);
$st7 = $db7->prepare("delete from posts where pid=?");


// iterate on results
while($row = mysqli_fetch_assoc($res)){
	$st2->bind_param('i', $row['pid']);
	$st2->execute();
	$res2 = $st2->get_result();
	if($res2){
		while($row2 = mysqli_fetch_assoc($res2)){
			$st3->bind_param('i', $row2['cid']);
			$st3->execute();
		}
	}

	$st4->bind_param('i', $row['pid']);
	$st4->execute();
	$st5->bind_param('i', $row['pid']);
	$st5->execute();
	$st6->bind_param('i', $row['pid']);
	$st6->execute();
	$st7->bind_param('i', $row['pid']);
	$st7->execute();
}

$db ->close();
$db2 ->close();
$db3 ->close();
$db4 ->close();
$db5 ->close();

// delete votes then user
printf("<h1>Deleting account</h1>", $u);
mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
$db = new mysqli($sqlhost, $sqluser, $sqlpass, $sqldb);
$st = $db->prepare("select uid from users where uid=?");
$st->bind_param('i', $_SESSION['uid']);
$st->execute();
$res = $st->get_result();
$row = $res->fetch_assoc();

// delete user
$db2 = new mysqli($sqlhost, $sqluser, $sqlpass, $sqldb);
$st2 = $db->prepare("delete from users where uid=?");
$st2->bind_param('i', $row['uid']);
$st2->execute();
$db1->close();
$db2->close();

printf("<h1>Account deleted!</h1>");
html_footer("");
header("refresh:1;url=/lib/do_logout.php");
die();
?>
