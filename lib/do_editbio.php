<?php
session_start();
include "htmlmodules.php";

$uid = $_SESSION['uid'];
$name = $_SESSION['user'];
$loggedin = $_SESSION['loggedin'];

$err = 0;

# grab user info (necessary)
mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
$db = new mysqli($sqlhost, $sqluser, $sqlpass, $sqldb);
$st = $db->prepare("select uid from users where uid=?");
$st->bind_param('i', $uid);
$st->execute();
$res = $st->get_result();
$row = $res->fetch_assoc();

# check that the current user owns this post and is logged in
if(!isset($loggedin))
	$err = 1;
       
if($loggedin != true)
	$err = 1;

if($row['uid'] != $_SESSION['uid'])
	$err = 2;


switch($err){
	case 0:
		continue;
	case 1:
		html_header("Not logged in");
		html_body("You are not logged in. Please refer to <a href='login.php'> to log in or wait to be redirected");
		header("refresh:3;url=/lib/login.php");
		die();
	case 2:
		html_header("Cannot edit post");
		html_body("You cannot edit a bio that you do not own ( ͡° ͜ʖ ͡°) ");
		die();
}


# insert post
$db = new mysqli($sqlhost, $sqluser, $sqlpass, $sqldb);
$st = $db->prepare("update users set bio=? where uid=?");
$st->bind_param('si', strip_tags($_POST['bio']), $_SESSION['uid']);
$st->execute();
$db->close;

html_header("Bio updated!");
html_body("Bio updated!");
html_footer("");
header("refresh:1;url=/lib/mypage.php");
die();
?>
