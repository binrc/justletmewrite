<?php
session_start();
include "htmlmodules.php";

$err = 0;

if(array_key_exists('uid', $_SESSION)){
	$uid = $_SESSION['uid'];
} else {
	$err = 1;
}

if(array_key_exists('user', $_SESSION))
	$name = $_SESSION['user'];

if(array_key_exists('loggedin', $_SESSION))
	$loggedin = $_SESSION['loggedin'];

$pid = $_GET['pid'];


# grab post info (necessary)
mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
$db = new mysqli($sqlhost, $sqluser, $sqlpass, $sqldb);
$st = $db->prepare("select pid,author,content,title from posts where pid=?");
$st->bind_param('i', $pid);
$st->execute();
$res = $st->get_result();
if($res){
	$row = $res->fetch_assoc();
} else {
	echo "sql broke" . $db->errno;
	die();
}
$db->close();

# check that the current user owns this post and is logged in
if(!isset($loggedin))
	$err = 1;
       
if($loggedin != true)
	$err = 1;

if($row['author'] != $_SESSION['uid'])
	$err = 2;

switch($err){
	case 0:
		continue;
	case 1:
		html_header("Not logged in");
		html_body("You are not logged in. Please refer to <a href='login.php'> to log in or wait to be redirected");
		header("refresh:3;url=/lib/login.php");
		die();
		break;
	case 2:
		html_header("Cannot delete post");
		html_body("You cannot delete a post that you did not write ( ͡° ͜ʖ ͡°) ");
		die();
		break;
}

# delete votes
$db = new mysqli($sqlhost, $sqluser, $sqlpass, $sqldb);
$st = $db->prepare('delete from ratings where post=?');
$st->bind_param('i', $row['pid']);
$st->execute();
$db->close();

# delete comment hashes then comments
$db = new mysqli($sqlhost, $sqluser, $sqlpass, $sqldb);
$st = $db->prepare('select cid from comments where post=?');
$st->bind_param('i', $row['pid']);
$st->execute();
$cres = $st->get_result();
$db->close();


$db = new mysqli($sqlhost, $sqluser, $sqlpass, $sqldb);
$dst = $db->prepare('delete from commenthashes where comment=?');
if($cres){
	while($crow = mysqli_fetch_assoc($res)){
		$dst->bind_param('i', $crow['cid']);
		$dst->execute();
	}
}

$db->close();

$db = new mysqli($sqlhost, $sqluser, $sqlpass, $sqldb);
$st = $db->prepare('delete from comments where post=?');
$st->bind_param('i', $row['pid']);
$st->execute();
$db->close();

# delete post hash then post
$db = new mysqli($sqlhost, $sqluser, $sqlpass, $sqldb);
$st = $db->prepare('delete from posthashes where post=?');
$st->bind_param('i', $row['pid']);
$st->execute();
$db->close();

$db = new mysqli($sqlhost, $sqluser, $sqlpass, $sqldb);
$st = $db->prepare('delete from posts where pid=?');
$st->bind_param('i', $row['pid']);
$st->execute();
$db->close();




html_header("Post deleted!");
html_body("Post deleted!");
html_footer("");
header("refresh:1;url=/lib/mypage.php");
die();
?>
