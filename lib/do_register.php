<?php
session_start();
include "htmlmodules.php";
html_header("Submit");

// initialize with no errors
$err = 0;

// check not null
if($_POST['username'] != NULL)
	$u = trim(strip_tags($_POST['username']));
else $err=1;

if($_POST['password'] != NULL)
	$p = $_POST['password'];
else $err=1;

if($_POST['password2'] != NULL)
	$p2 = $_POST['password2'];
else $err=1;

if($_POST['bio'] != NULL)
	$bio = strip_tags($_POST['bio']);
else $err=1;

// check passwd match
if($p != $p2)
	$err = 2;

// enforce alphanumeric characters, _, and -
if(preg_match('/[^a-z_\-0-9]/i', trim(strip_tags($_POST['username']))))
	$err = 4;

// check if username is taken
mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
$db = new mysqli($sqlhost, $sqluser, $sqlpass, $sqldb);
$st = $db->prepare("select count(*) from users where name = ?");
$st->bind_param('s', $u);
$st->execute();
$res = $st->get_result();
$row = $res->fetch_assoc();
$db->close();

if($row['count(*)'] != 0)
	$err = 3;

// handle errors
switch($err){
case 0:
	continue;
case 1: 
	die("Error: Username/password/bio cannot be blank\n");
	break;
case 2:
	die("Error: Passwords do not match\n");
	break;
case 3:
	die("Error: Username already exists\n");
	break;
case 3:
	die("Only alphanumeric characers, _, and - are allowed in usernames");
	break;
}

// create password hash
$p = $p . $salt; // append salt
$p = hash('sha512', $p);

// create user
mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
$db = new mysqli($sqlhost, $sqluser, $sqlpass, $sqldb);
$st = $db->prepare("insert into users(bio, name, passhash, fg, bg, ac) values(?,?,?, '#000000', '#ffffff', '#1010ff')");
$st->bind_param('sss', $bio, $u, $p);
$st->execute();
$db->close();

// display some bs
printf("<h1>%s has been created</h1>", $u);
printf("<p>Wait to be redirected or click <a href='login.php'>login</a> if it does not work. </p>");

html_footer("");
header('refresh:3;url=/lib/login.php');
die();
?>
