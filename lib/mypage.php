<?php
session_start();
include "htmlmodules.php";

$uid = $_SESSION['uid'];
$name = $_SESSION['user'];
$loggedin = $_SESSION['loggedin'];

$err = 0;

if(!isset($loggedin))
	$err = 1;
       
if($loggedin != true)
	$err = 1;


switch($err){
	case 0:
		continue;
	case 1:
		html_header("Not logged in");
		html_body("You are not logged in. Please refer to <a href='login.php'> to log in or wait to be redirected");
		header("refresh:1;url=/lib/login.php");
		break;
}

html_header($name . "'s page (edit mode)");

# grab user info (necessary)
mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
$db = new mysqli($sqlhost, $sqluser, $sqlpass, $sqldb);
$st = $db->prepare("select uid,name,bio,fg,bg,ac from users where uid=?");
$st->bind_param('i', $uid);
$st->execute();
$res = $st->get_result();
$userrow = $res->fetch_assoc();
$db->close();


printf("<div><h1>%s's page <a href='rss.php?uid=%s'><img style='display: inline;' alt='rss feed logo' src='/assets/images/rss.png'></a></h1><blockquote>%s<a href='editbio.php'> (edit)</a></blockquote></div>", $userrow['name'], $userrow['uid'], $userrow['bio']);

# post creator

printf("<div><h3>Create new post</h3><form action='do_newpost.php' method='post' id='newpost'><label for='title'>Title: </label><input type='text' name='title' id='title' required autocomplete='off'><br><label for='post'>Write something: <br></label><textarea minlength='2000' name='post' form='newpost' id='post' rows='24' cols='40' wrap='soft' required></textarea><br>
<label for='license'>Choose a license: </label>
<select name='license' id='license'>
<option value='%s'>Default: %s</option>
<option value='copyright'>copyright</option>
<option value='copyleft'>copyleft</option>
<option value='cc-zero'>cc-zero</option>
<option value='cc-by'>cc-by</option>
<option value='cc-by-sa'>cc-by-sa</option>
<option value='cc-by-nc'>cc-by-nc</option>
<option value='cc-by-nc-sa'>cc-by-nc-sa</option>
<option value='cc-by-nd'>cc-by-nd</option>
<option value='cc-by-nc-nd'>cc-by-nc-nd</option>
</select>
<button type='submit'>Save Post</button></form></div>", $_SESSION['license'], $_SESSION['license']);

// counters
printf("<div><h3>Your Posts</h3>");
mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
$db = new mysqli($sqlhost, $sqluser, $sqlpass, $sqldb);
$st = $db->prepare("select count(*) from posts where author=?;");
$st->bind_param("i", $uid);
$st->execute();
$res = $st->get_result();
$row = mysqli_fetch_array($res);
$post_count = $row['count(*)'];
$db->close();


$db = new mysqli($sqlhost, $sqluser, $sqlpass, $sqldb);
$st = $db->prepare("select count(*) from comments where author=?;");
$st->bind_param("i", $uid);
$st->execute();
$res = $st->get_result();
$row = mysqli_fetch_array($res);
$comment_count = $row['count(*)'];
$db->close();

printf("<p>You have posted %s time(s) and commented %s time(s)</p>", $post_count, $comment_count);

// list posts
$db = new mysqli($sqlhost, $sqluser, $sqlpass, $sqldb);
$st = $db->prepare("select title,pid,dt from posts where author=? order by pid desc;");
$st->bind_param("i", $uid);
$st->execute();
$res = $st->get_result();

if($res){
	while($row = mysqli_fetch_array($res)){
		printf("<a href='post.php?pid=%s'>%s</a> - %s&nbsp;&nbsp;<a href='editpost.php?pid=%s'>(edit)</a> &nbsp;&nbsp;<a href='deletepost.php?pid=%s'>(DELETE)</a><br>", $row['pid'], $row['title'], $row['dt'], $row['pid'], $row['pid']);
	}
}

echo "</div></div>";

# color chooser

printf("<hr><div class='col-12'><h3>Account Settings</h3><div class='mypage'>
<form action='do_settheme.php' method='post' id='theme'>
<p> Set your theme </p>
<label for='fg'>Foreground: </label><input type='color' name='fg' id='fg' value='%s'><br>
<label for='bg'>Background: </label><input type='color' name='bg' id='bg' value='%s'><br>
<label for='ac'>Accent color: </label><input type='color' name='ac' id='ac' value='%s'><br>
<button type='submit'>Save theme</button>
</form></div>", $userrow['fg'], $userrow['bg'], $userrow['ac']);

# defualt license chooser

printf("<div class='mypage'>
<form action='do_setlicense.php' method='post' id='license'>
<p>Choose a default license </p>
<select name='license' id='license'>
<option value='%s'>Default: %s</option>
<option value='copyright'>copyright</option>
<option value='copyleft'>copyleft</option>
<option value='cc-zero'>cc-zero</option>
<option value='cc-by'>cc-by</option>
<option value='cc-by-sa'>cc-by-sa</option>
<option value='cc-by-nc'>cc-by-nc</option>
<option value='cc-by-nc-sa'>cc-by-nc-sa</option>
<option value='cc-by-nd'>cc-by-nd</option>
<option value='cc-by-nc-nd'>cc-by-nc-nd</option>
</select>
<button type='submit'>Save license</button>
</form></div>", $_SESSION['license'], $_SESSION['license']);

// export posts
echo "<div class='mypage'><form><p>Export posts</p><button><a href='do_exportmarkdownzip.php' target='_blank'>.zip</a></button> <button><a href='do_exportcsv.php' target='_blank'>CSV</a></button></form></div>";

# account nuker
printf("<div class='mypage'><form><p> Account deletion </p><button><a href='do_deleteaccount.php'>Delete my account</a></button></form></div>");




$db->close();
html_footer("");
?>
