<?php
session_start();
include "htmlmodules.php";
html_header("Authors");
// get passhas from db
mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
$db = new mysqli($sqlhost, $sqluser, $sqlpass, $sqldb);
$res = $db->query("select pid from posts order by rand() limit 1");
$row = mysqli_fetch_array($res);
$db->close();

html_body("<p>Redirecting . . . If this does not work you can click the <a href='/lib/post.php?pid=" . $row['pid'] . "'>link</a> manually");
html_footer("");
header("refresh:1;url=/lib/post.php?pid=" . $row['pid']);
die();
?>
