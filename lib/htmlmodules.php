<?php
include "../../serverconf.php";
include "markdown.php";

function set_style(){
	include "../../serverconf.php";
	if(isset($_SESSION['uid'])){
		# grab colors
		mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
		$db = new mysqli($sqlhost, $sqluser, $sqlpass, $sqldb);
		$st = $db->prepare("select fg,bg,ac from users where uid=?");
		$st->bind_param('i', $_SESSION['uid']);
		$st->execute();
		$res = $st->get_result();
		$row = $res->fetch_assoc();
		printf("<style>:root{--fg: %s !important; --bg: %s !important; --ac: %s !important;}</style>", $row['fg'], $row['bg'], $row['ac']);
	}

}

function html_header($title){
	printf('<!DOCTYPE html><html lang="en"><head> <link rel="stylesheet" type="text/css" href="/assets/custom.css"><meta charset="UTF-8"><meta name="viewport" content="width=device-width, initial-scale=1.0">');
	set_style();
	printf("<title>%s</title>", $title);
	printf('</head><body><nav> | <a class="navtitle" href="/index.php">Home</a> | <a class="navlink" href="/lib/authors.php">Authors</a> | <a class="navlink" href="/lib/random.php">Random</a> | <a class="navlink" href="/lib/search.php">Search</a> | </nav> <hr> <div class="content"><div class="row"><div class="col-12">');

};

function html_body($content){
	printf("<div class='general-body'>%s</div>", $content);
};

function html_post($content){
	printf("<div class='post'>");
	markdown($content);
	printf("</div>");
};

function html_footer($js){
	printf('
</div>
</div>
</div>
<hr>
<div class="row">
	<div class="col-12 footer"><nav> | <a class="navlink" href="/lib/register.php">Register</a> | <a class="navlink" href="/lib/login.php">Login</a> | <a class="navlink" href="/lib/do_logout.php">Logout</a> | <a href="/lib/mypage.php">My Page </a> | </nav>
	</div>
</div>
<!-- js here -->
</body>
</html>
');
	// javascript footer
	printf($js);
	printf("</body></html>");
	include "session_regen.php";//regenerate session key on every page reload
};

function html_search_box(){
	printf('<form action="do_search.php" method="post" id="search">
<h1> Search </h1>
<label for="query">Query: </label>
<input type="text" name="query" id="query" required autocomplete="on">
<button type="submit">Submit</button>
</form>');
};

function html_comment_section($pid){
	include "../../serverconf.php";
	printf("<br>");
		# grab comments
		mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
		$db = new mysqli($sqlhost, $sqluser, $sqlpass, $sqldb);
		$st = $db->prepare("select users.name,comments.* from comments join users on comments.author=users.uid where post=?;");

		$st->bind_param('i', $pid);
		$st->execute();
		$res = $st->get_result();
		if($res){
			while($row = mysqli_fetch_array($res)){
				printf("<div class='posttitle'><a href='authors.php?id=%s'>%s</a> - %s</div><div class='comment'><p>%s</p></div>", $row['author'], $row['name'], $row['dt'], $row['content']);
			}
		}

		# add comment
}

function html_comment_form($pid){
	printf("<hr><div class='postpage'><form action='do_addcomment.php?pid=%s' method='post' id='addcomment'>
	<label for='comment'>Add a comment: <br></label>
	<textarea minlength='500' name='comment' form='addcomment' id='comment' rows='12' cols='40' wrap='soft' required></textarea><br>
	<button type='submit'>Save</button></form></div>", $pid);
}
		

function get_letter_grade($grade){
	$grades = array(
		// letter, lower bound, upper bound
		array("S", 100, 100),
		array("A+", 97, 100),
		array("A", 93, 96),
		array("A-", 90, 92),
		array("B+", 87, 89),
		array("B", 83, 86),
		array("B-", 80, 82),
		array("C+", 77, 79),
		array("C", 73, 76),
		array("C-", 70, 72),
		array("D+", 67, 69),
		array("D", 63, 66),
		array("D-", 60, 62),
		array("F", 0, 59)
	);
	for($i = 0; $i < count($grades); $i++){
		if($grade <= $grades[$i][2] && $grade >= $grades[$i][1])
			$letter = $grades[$i][0];
	}
	
	return $letter;
}

function html_vote_system($pid){
	include "../../serverconf.php";
	printf("<br><hr><div class='postpage'>");
		# grab comments
		mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
		$db = new mysqli($sqlhost, $sqluser, $sqlpass, $sqldb);
		$st = $db->prepare("select count(rating) from ratings where post=?");
		$st->bind_param('i', $pid);
		$st->execute();
		$res = $st->get_result();
		$row = mysqli_fetch_array($res);
		$db->close();

		if($row['count(rating)'] == 0) {
			printf("This post has no votes. ");
		} else {
			mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
			$db = new mysqli($sqlhost, $sqluser, $sqlpass, $sqldb);
			$st = $db->prepare("select rating from ratings where post=?");
			$st->bind_param('i', $pid);
			$st->execute();
			$res = $st->get_result();
			$db->close();
			if($row){
				$count = 0;
				while($row = mysqli_fetch_array($res)){
					$grade = $grade+$row['rating'];
					$count = $count + 1;
				}

				$grade = $grade/$count;
				printf("<span>Community grade: %s%% %s</span>", $grade, get_letter_grade($grade));
			}
		}


		# add vote
	printf("<form action='do_addrating.php?pid=%s' method='post' id='vote'>
	<label for='rating'>Rate on a scale from 0 to 100: <br></label>
	<input id='rating' name='rating' type='number' min='0' max='100'>
	<button type='submit'>Submit</button></form></div>", $pid);
		
}

function html_license($license){
	$licenses = array(
		array("copyright",  "https://en.wikipedia.org/wiki/Copyright"),
		array("copyleft", "https://copyleft.org/"),
		array("publicdomain", "https://en.wikipedia.org/wiki/Public_domain"),
		array("cc-zero", "https://creativecommons.org/publicdomain/zero/1.0/"),
		array("cc-by", "https://creativecommons.org/licenses/by/4.0/"),
		array("cc-by-nc", "https://creativecommons.org/licenses/by-nc/4.0/"),
		array("cc-by-nc-nd", "https://creativecommons.org/licenses/by-nc-nd/4.0"),
		array("cc-by-nc-sa", "https://creativecommons.org/licenses/by-nc-sa/4.0"),
		array("cc-by-nd", "https://creativecommons.org/licenses/by-nd/4.0"),
		array("cc-by-sa", "https://creativecommons.org/licenses/by-sa/4.0"));

	foreach($licenses as $l){
		if(!strcmp($license, $l[0])){
			printf(" - <a class='licenseimg' href='%s'><img src='/assets/images/%s.png' alt='%s'></a>", $l[1], $l[0], $l[0]);
		}
	}

	return;
}

//  session debug
if($debug_session == 1){
	echo "SERVER NAME: " . $_SERVER['SERVER_NAME'] . "<br>";
	printf("SESSION ID: %s<br>", session_id());
	echo "SESSION UID: " . $_SESSION['uid'] . "<br>";
	echo "SESSION NAME: " . $_SESSION['user'] . "<br>";
	print_r(session_get_cookie_params());
}
?>
