<?php
session_start();
include "htmlmodules.php";

$uid = $_SESSION['uid'];

# grab user info
mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
$db = new mysqli($sqlhost, $sqluser, $sqlpass, $sqldb);
$st = $db->prepare("select pid,title,dt,content from posts where author=?");
$st->bind_param('i', $uid);
$st->execute();
$res = $st->get_result();
$db->close();

// zip nonsense
$tmp = tmpfile();
$tmp_path = stream_get_meta_data($tmp)['uri'];

$zip = new ZipArchive;

$zip_file = $zip->open($tmp_path, ZipArchive::CREATE);


while($row = mysqli_fetch_assoc($res)){
	$name = $row['pid'] . ".md";
	$content = "---\ntitle:  " . $row['title'] . "\ndate: " . $row['dt'] . "\n---\n\n" . $row['content'];
	$zip->addFromString($name, $content);
}

$zip->close();

header('Content-type: application/zip');
header('Content-Disposition: attachment; filename="export.zip"');
echo(file_get_contents($tmp_path));


?>
