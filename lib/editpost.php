<?php
session_start();
include "htmlmodules.php";

$uid = $_SESSION['uid'];
$name = $_SESSION['user'];
$loggedin = $_SESSION['loggedin'];
$pid = $_GET['pid'];

$err = 0;

# grab post info (necessary)
mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
$db = new mysqli($sqlhost, $sqluser, $sqlpass, $sqldb);
$st = $db->prepare("select license,author,content,title from posts where pid=?");
$st->bind_param('i', $pid);
$st->execute();
$res = $st->get_result();
if($res){
	$row = $res->fetch_assoc();
} else {
	echo "sql broke" . $db->errno;
	die();
}
$db->close();

# check that the current user owns this post and is logged in
if(!isset($loggedin))
	$err = 1;
       
if($loggedin != true)
	$err = 1;

if($row['author'] != $_SESSION['uid'])
	$err = 2;


switch($err){
	case 0:
		continue;
	case 1:
		html_header("Not logged in");
		html_body("You are not logged in. Please refer to <a href='login.php'> to log in or wait to be redirected");
		header("refresh:3;url=/lib/login.php");
		die();
	case 2:
		html_header("Cannot edit post");
		html_body("You cannot edit a post that you did not write ( ͡° ͜ʖ ͡°) ");
		die();
}

# generate form
html_header($row['title'] . " (edit)");
printf("<h1>%s (edit)</h1>
<form action='do_editpost.php?pid=%s' method='post' id='editpost'>
<label for='title'>Title: </label>
<input type='text' name='title' id='title' required autocomplete='off' value='%s'><br>
<label for='post'>Write something: <br></label>
<textarea minlength='2000' name='post' form='editpost' id='post' rows='24' cols='40' wrap='soft' required>%s</textarea><br>
<label for='license'>Choose a license</label>
<select name='license' id='license'>
<option value='%s'>Default: %s</option>
<option value='copyright'>copyright</option>
<option value='copyleft'>copyleft</option>
<option value='cc-zero'>cc-zero</option>
<option value='cc-by'>cc-by</option>
<option value='cc-by-sa'>cc-by-sa</option>
<option value='cc-by-nc'>cc-by-nc</option>
<option value='cc-by-nc-sa'>cc-by-nc-sa</option>
<option value='cc-by-nd'>cc-by-nd</option>
<option value='cc-by-nc-nd'>cc-by-nc-nd</option>
</select><button type='submit'>Save Post</button></form>", $row['title'], $pid, $row['title'], $row['content'], $row['license'], $row['license']);

html_footer("");
?>
