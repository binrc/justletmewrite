<?php
session_start();
include "htmlmodules.php";

$author = $_GET['uid'];

# grab user info
mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
$db = new mysqli($sqlhost, $sqluser, $sqlpass, $sqldb);
$st = $db->prepare("select uid,name,bio from users where uid=?");
$st->bind_param('i', $author);
$st->execute();
$res = $st->get_result();
$row = $res->fetch_assoc();
$db->close();

$author = $row['name'];
html_header($author . "'s page");
printf("<h1>%s's page <a href='rss.php?uid=%s'><img style='display: inline;' alt='rss feed logo' src='/assets/images/rss.png'></a></h1><blockquote>%s</blockquote><p>", $row['name'], $row['uid'], $row['bio']);

// not abandoning prepared statements here because the uid value came from the database, not the user
$uid = $row['uid'];

// redirect to mypage if I am viewing my author page
if(array_key_exists('uid', $_SESSION) && $uid == $_SESSION['uid']){
	header("refresh:1;url=/lib/mypage.php");
	die();
}

// counters
mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
$db = new mysqli($sqlhost, $sqluser, $sqlpass, $sqldb);
$st = $db->prepare("select count(*) from posts where author=?;");
$st->bind_param("i", $uid);
$st->execute();
$res = $st->get_result();
$row = mysqli_fetch_array($res);
$post_count = $row['count(*)'];
$db->close();


$db = new mysqli($sqlhost, $sqluser, $sqlpass, $sqldb);
$st = $db->prepare("select count(*) from comments where author=?;");
$st->bind_param("i", $uid);
$st->execute();
$res = $st->get_result();
$row = mysqli_fetch_array($res);
$comment_count = $row['count(*)'];
$db->close();

printf("<p>This user has posted %s time(s) and commented %s time(s)</p>", $post_count, $comment_count);

// list posts
$db = new mysqli($sqlhost, $sqluser, $sqlpass, $sqldb);
$st = $db->prepare("select title,pid,dt from posts where author=? order by pid desc;");
$st->bind_param("i", $uid);
$st->execute();
$res = $st->get_result();

if($res){
	while($row = mysqli_fetch_array($res)){
		printf("<a href='post.php?pid=%s'>%s</a> - %s<br>", $row['pid'], $row['title'], $row['dt']);
	}
}

$db->close();

html_footer("");
?>
