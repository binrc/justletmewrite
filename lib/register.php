<?php
session_start();
include "htmlmodules.php";
html_header("Registration");

html_body('<form action="do_register.php" method="post" id="registerform">
<h1> Registration </h1>
<label for="username">Username (whitespace will be stripped): </label>
<input type="text" name="username" id="username" pattern="[a-zA-Z0-9]+" required autocomplete="off">
</br>
<label for="password">Password: </label>
<input type="password" name="password" id="password" required autocomplete="off">
<br>
<label for="password2">Password Again: </label>
<input type="password" name="password2" id="password2" required autocomplete="off">
<br>
<label for="bio">About you: </label><br>
<textarea form ="registerform" name="bio" id="bio" rows="12" cols="40" wrap="soft" required></textarea>
<br>
<button type="submit">Register</button>
</form>');

html_footer("");
?>
