<?php
session_start();
include "htmlmodules.php";

$uid = $_SESSION['uid'];
$name = $_SESSION['user'];
$loggedin = $_SESSION['loggedin'];

$err = 0;

# grab user info (necessary)
mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
$db = new mysqli($sqlhost, $sqluser, $sqlpass, $sqldb);
$st = $db->prepare("select uid from users where uid=?");
$st->bind_param('i', $uid);
$st->execute();
$res = $st->get_result();
$row = $res->fetch_assoc();

# check that the current user owns this post and is logged in
if(!isset($loggedin))
	$err = 1;
       
if($loggedin != true)
	$err = 1;

if($row['uid'] != $_SESSION['uid'])
	$err = 2;


switch($err){
	case 0:
		continue;
	case 1:
		html_header("Not logged in");
		html_body("You are not logged in. Please refer to <a href='login.php'> to log in or wait to be redirected");
		header("refresh:3;url=/lib/login.php");
		die();
	case 2:
		html_header("Cannot delete account");
		html_body("You cannot delete an account you do not own ( ͡° ͜ʖ ͡°) ");
		die();
}


html_header("Account deletion");
# confirm 
html_body('<form action="do_reallydeleteaccount.php" method="post" id="login">
<h1> Please enter your username and password to confirm </h1>
<label for="username">Username: </label>
<input type="text" name="username" id="username" pattern="[a-zA-Z0-9]+" required autocomplete="off">
</br>
<label for="password">Password: </label>
<input type="password" name="password" id="password" required autocomplete="off">
<br>
<label for="reallydelete">Really delete your account?</label>
<input type="checkbox" id="reallydelete" name="reallydelete" required>
<button type="submit">Submit</button>
</form>');

html_footer("");
die();
?>
