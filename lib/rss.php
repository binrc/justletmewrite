<?php
include "htmlmodules.php";

$uid = $_GET['uid'];


header("Content-type: application/xml; charset=utf-8");
echo "<?xml version='1.0' encoding='utf-8'?>\n";
echo "<rss version='2.0' xmlns:atom='http://www.w3.org/2005/Atom'>\n";
echo "<channel>\n";

// feeds for all posts
if($uid == 'all' || empty($_GET)){
	printf("<title>%s rss feed</title>
		<link>%s</link>
		<atom:link href='%s/lib/rss.php?uid=all' rel='self' type='application/rss+xml' />
		<description>%s rss feed</description>", $_SERVER['SERVER_NAME'], $_SERVER['SERVER_NAME'], $_SERVER['SERVER_NAME'], $_SERVER['SERVER_NAME']);

		mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
		$db = new mysqli($sqlhost, $sqluser, $sqlpass, $sqldb);
		$st = $db->prepare("select title,dt,content,author,pid from posts order by dt desc limit 50;");
		$st->execute();
		$res = $st->get_result();
		$db->close();
		if($res){
			// prepared statement for looping
			$db = new mysqli($sqlhost, $sqluser, $sqlpass, $sqldb);
			$st = $db->prepare("select name from users where uid=? limit 1;");

			while($row = mysqli_fetch_assoc($res)){
				$uri = $_SERVER['SERVER_NAME'] . "/lib/post.php?pid=" . $row['pid'];

				// get user's name
				$st->bind_param('i', $row['author']);
				$st->execute();
				$nameres = $st->get_result();
				$name = mysqli_fetch_assoc($nameres);

				// print items
				printf("<item>
					<guid>%s</guid>
					<title>%s</title>
					<link>%s</link>
					<author>%s</author>
					<pubDate>%s</pubDate>", $uri, $row['title'], $uri, $name['name'], $row['dt']);


				echo "<description><![CDATA[\n";
				markdown($row['content']);
			echo "]]></description>\n</item>";
			}
		}

		echo "</channel>\n</rss>\n";
		$db->close();

} else {	// feeds for single user
		// get posts
		mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
		$db = new mysqli($sqlhost, $sqluser, $sqlpass, $sqldb);
		$st = $db->prepare("select title,dt,content,author,pid from posts where author=? order by dt desc limit 50;");
		$st->bind_param('i', $uid);
		$st->execute();
		$res = $st->get_result();
		$db->close();

		// get user's name
		$db = new mysqli($sqlhost, $sqluser, $sqlpass, $sqldb);
		$st2 = $db->prepare("select name from users where uid=? limit 1;");
		$st2->bind_param('i', $uid);
		$st2->execute();
		$nameres = $st2->get_result();
		$name = mysqli_fetch_assoc($nameres);
		$db->close();

		printf("<title>%s rss feed from %s</title>
		<link>%s/author.php?uid=%s</link>
		<atom:link href='http://%s/lib/rss.php?uid=%s' rel='self' type='application/rss+xml' />
		<description>%s rss feed</description>", $name['name'],$_SERVER['SERVER_NAME'], $_SERVER['SERVER_NAME'], $uid, $_SERVER['SERVER_NAME'], $uid, $name['name']);


		if($res){

			while($row = mysqli_fetch_assoc($res)){
				$uri = $_SERVER['SERVER_NAME'] . "/lib/post.php?pid=" . $row['pid'];

				// print items
				printf("<item>
					<guid>%s</guid>
					<title>%s</title>
					<link>%s</link>
					<author>%s</author>
					<pubDate>%s</pubDate>", $uri, $row['title'], $uri, $name['name'], $row['dt']);


				echo "<description><![CDATA[\n";
				markdown($row['content']);
			echo "]]></description>\n</item>";
			}
		}

		echo "</channel>\n</rss>\n";


}
?>
