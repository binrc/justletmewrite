<?php
session_start();
include "htmlmodules.php";

$uid = $_SESSION['uid'];

# grab user info
mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
$db = new mysqli($sqlhost, $sqluser, $sqlpass, $sqldb);
$st = $db->prepare("select title,dt,content from posts where author=?");
$st->bind_param('i', $uid);
$st->execute();
$res = $st->get_result();

header("Content-Type: application/octet-stream");
header("Content-Transfer-Encoding: Binary");
header("Content-disposition: attachment; filename=\"export.csv\"");

$output = fopen("php://output", "w");


fputcsv($output, array("Title", "Date", "Content"));
while($row = mysqli_fetch_assoc($res)){
	fputcsv($output, $row);
}

fclose($output);

$db->close();

?>
