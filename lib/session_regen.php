<?php
if(!isset($_SESSION['regen'])){
	session_regenerate_id(true);
	$_SESSION['last_regen'] = time();
} else {
	// regen session id every 30 minutes
	if(time() - $_SESSION['last_regen'] >= (60*30)) {
		session_regenerate_id(true);
		$_SESSION['last_regeneration'] = time();
	}
}
?>
