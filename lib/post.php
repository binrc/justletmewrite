<?php
session_start();
include "htmlmodules.php";

$post = $_GET['pid'];

# grab user info
mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
$db = new mysqli($sqlhost, $sqluser, $sqlpass, $sqldb);
$st = $db->prepare("select pid,content,author,title,dt,license from posts where pid=?");
$st->bind_param('i', $post);
$st->execute();
$res = $st->get_result();
$row = $res->fetch_assoc();
$db->close();


mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
$db = new mysqli($sqlhost, $sqluser, $sqlpass, $sqldb);
$st = $db->prepare("select name from users where uid=?;");
$st->bind_param("i", $row['author']);
$st->execute();
$res2 = $st->get_result();
$row2 = mysqli_fetch_array($res2);
$db->close();

html_header($row['title'] .  " - " . $row2['name']);
printf("<h1 class='posttitle'>%s - <a href='author.php?uid=%s'>%s</a> - %s", $row['title'], $row['author'], $row2['name'], $row['dt']);
if(strlen($row['license'] > 0)){
	html_license($row['license']);
}
echo "</h1>";

html_post($row['content']);
html_vote_system($post);
html_comment_form($post);
html_comment_section($post);
html_footer("");
?>
