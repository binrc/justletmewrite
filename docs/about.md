<div class="sitepage">
# About

While thinking about social media, I realized that all existing social media shares similar issues. These issues include privacy concerns, platform enforced censorship, and low quality content (spam, ai generated trash, influencer garbage, thinly guised advertisements). Additionally, the websites that have a voting system create user enforced censorship. 

In order to remedy these anti features, certain features have been implemented. 

To address the privacy concerns, the system lacks the ability to upload images entirely. Users cannot upload their picture directly and are discouraged from using their real names. 

Platform censorship is wholly nonexistent because it is very difficult to create something that is actually illegal in the US using just text. 

Low quality content and spam remedied in two ways: all posts and comments must be original (unique) and a minimum character limit is enforced (2000 for posts, 500 for comments). The concept is that high effort posting requirements will increase the barrier to entry. Effort != quality but nothing of value has ever been said in 280 characters or less. Originality, on the other hand, prevents lazy spammers. 

The voting system exists as a satire of other platforms where voting systems degrade into popularity mechanics. Posts are rated by users on a scale from 0 to 100 and a grade is calculated. Posts are only sorted by date descending so the voting system serves only to briefly inform the reader of the quality of the post before they find themselves filled with regret after reading halfway through a 10000 character long post.  

Certain quality of life features have been included. The definiton of QOL here is *absence of anti-features* and the presence of *things we actually want*. This software is entirely JS-free, login is not required to view posts, it supports markdown (to some extent), and enables the users to change the colorscheme of the website to their own taste. This website will never have ads and is provided for free of charge. 

# Data Policy

Every time you connect to this server your IP address, time of access, and request string is stored by httpd for server security purposes. This is a standard security practice. These are not inserted into the database. An example log looks like this: 

```
https://justletmewrite.org 127.0.0.1 - - [24/Jan/2024:23:56:41 +0000] "GET /index.php HTTP/1.1" 200 0
```

When you make an account, everything you submit under that account is stored in the databse. You have the option to export your posts in a CSV or as individual markdown files. Currently, you can delete individual posts but not individual comments or votes. You can, however, delete everything associated with your user along with your account. The full deletion is not perfectly implemented. 

No other information that is not strictly necessary for the operation and functionality of the website is stored. 

# General Rules

> don't hax pls

if you find a vulnerability, please submit it (or a patch) at [the project's git repository](https://gitlab.com/binrc/justletmewrite)

# Server Software License

this software is licensed under the [BSD 2 Clause](license.php);

# Licensing for Individual Posts

Licensing for creative works is important. Since all posts on this website are *at least* a couple of paragraphs long, each post is long enough to justify the use of a license. This website has a simple system that allows users to select a license for your individual posts and a 'default license' system that automatically sets you preferred license. 

Refer to [the licensing page](/licensing.php) for a brief explanation of the licenses you can choose. 

If you would like a license that is not implemented, please submit a patch or request at [the project's git repository](https://gitlab.com/binrc/justletmewrite)

# RSS

An rss feed containing all posts on the site can be accessed by either [https://justletmewrite.org/lib/rss.php](https://justletmewrite.org/lib/rss.php) or [https://justletmewrite.org/lib/rss.php?uid=all](https://justletmewrite.org/lib/rss.php?uid=all).


Per user rss feeds are accessed by [https://justletmewrite.org/lib/rss.php?uid=(your id here)](https://justletmewrite.org/lib/rss.php?uid=2). The link to an individual user's rss feed is on their author page. 
</div>
