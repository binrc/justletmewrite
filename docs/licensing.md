<div class="sitepage">
# Licensing for Creative Works

## Creative Commons Licenses

Creative commons licenses allow you to share your work in a simple and standardized way that enable others to use, share, and modify your work on a set of conditions that you choose. 

Mix and match from the table. Typically, it is wise to at least include BY with any other license so that derivative works and/or copies are attributed to you, the original author. SA and ND are mutually exclusive. 

| abbreviation | full name | rights granted |
| :--- | :--- | :--- |
| BY | attribution | anyone reusing, copying, or creating a derivative of your work must provide appropriate credit, provide a link to the license, and indicate if changes were made. The licensor does not endorse your work or your use. |
| SA | share alike | if you create a derivative work you must distribute your new version under the same license as the original. | 
| NC | non commercial | you may not use the material for commercial purposes. | 
| ND | no derivatives | you are not permitted to distribute derivative works. | 

An example license looks like `CC BY-SA`. This is a creative commons license that allows others to use, modify, and redistribute your work so long as they give you proper attribution. If you add replace `SA` with `ND`, derivative works are not permitted. If you add `NC`, anyone using, modifying, or redistributing your work cannot use it for commercial purposes (ie use it to make money). 


`CC0` is another creative commons license that allows you to dedicate your work to the public domain.

For more information, refer to [the creative commons website](https://creativecommons.org/share-your-work/cclicenses/)

## Public Domain

Works in the public domain are not actually licensed. Material in the public domain is not copyrighted and no license is required. Work placed in the public domain grants everyone all permissions without any restrictions. Attribution is not required. 

For more information, refer to [the wikipedia article about public domain](https://en.wikipedia.org/wiki/Public&#95;domain)

## Copyright

Copyright is, in simple terms, a license that means means "this is my intellectual property and I have exclusive rights to distribute, modify, and sell it". Copyright law is not internationally uniform. When copyright expires (typically after 50 years) the work enters the public domain. 

NOTE: Anything unlicensed is implicitly placed under copyright. 

## Copyleft

The copyleft license is nearly the same as the [FSF's four essential freedoms](https://www.gnu.org/philosophy/free-sw.en.html#four-freedoms) or a `CC-BY-SA` license. For more information, refer to [the wikipedia article about copyleft](https://en.wikipedia.org/wiki/Copyleft)
</div>
