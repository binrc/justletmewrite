- [X] account system (this will be the hard part)
    - [x] account creation
        - server side salt, sha512
        - enforce unique usernames
    - [x] account login
        - php sessions
    - [x] account logout

- [X] author pages
        - simple profile, list of all posts

- [x] plaintext post
    - [x] post editor

- [x] bio editor

- [x] settings page
        - [x] theme chooser (fg/bg colors) for unauthenticated users. This cookie will not expire and will be used by js to change the theme on the fly. 
        - [x] user account settings (change bio) 

- [x] search
    - [x] simple full text
    - [x] pull data from other tables (user table) instead of just title 

- [x] plaintext comments

- [x] markdown posts

- [x] ROBOT9000 on posts

- [x] ROBOT9000 on comments

- [x] voting system

- [x] de-uglify 

- [ ] mascot post counter

- [ ] de-spaghettify

- [ ] Write installation instructions
