CREATE user IF NOT EXISTS www@localhost IDENTIFIED BY 'password';
CREATE database IF NOT EXISTS www;

CREATE table IF NOT EXISTS www.users(
    uid INT PRIMARY KEY AUTO_INCREMENT NOT NULL, 
    bio TEXT NOT NULL,
    name TEXT NOT NULL unique,
    -- html includes the # for some reason
    fg varchar(7),
    bg varchar(7),
    ac varchar(7),
    license TEXT,
    -- password is sha512 hash
    passhash varchar(128) not null
    
);

CREATE table IF NOT EXISTS www.posts(
    pid INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    author INT NOT NULL,
    content text NOT NULL,
    title text NOT NULL,
    license TEXT,
    dt datetime default current_timestamp on update current_timestamp not null,
    FOREIGN KEY(author) REFERENCES users(uid),
    -- set minimum character limit to 2000 --CHECK( --length(content) >= 2000         --),
    fulltext(title, content)
);

CREATE table IF NOT EXISTS www.comments(
    cid INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    post INT NOT NULL,
    author INT NOT NULL,
    content TEXT NOT NULL,
    dt datetime default current_timestamp on update current_timestamp not null,
    FOREIGN KEY(author) REFERENCES users(uid),
    FOREIGN KEY(post) REFERENCES posts(pid)
    -- set minimum comment limit length to 500 --CHECK( --length(content) >= 500 --)
);

CREATE table IF NOT EXISTS www.ratings(
    rating INT NOT NULL,
    post INT NOT NULL,
    voter INT NOT NULL,
    FOREIGN KEY(post) REFERENCES posts(pid),
    FOREIGN KEY(voter) REFERENCES users(uid),
    unique key thekey (post,voter),
    CHECK(
        rating >= 0 AND
        rating <= 100
    )
);

-- sha256 hashes are 64 characters long, md5 collides too often
CREATE table IF NOT EXISTS www.posthashes(
    phash VARCHAR(64) PRIMARY KEY NOT NULL,
    post INT NOT NULL,
    FOREIGN KEY(post) REFERENCES posts(pid)
);

CREATE table IF NOT EXISTS www.commenthashes(
    chash VARCHAR(64) PRIMARY KEY NOT NULL,
    comment INT NOT NULL,
    FOREIGN KEY(comment) REFERENCES comments(cid)
);

insert into www.users(name, bio, passhash) values("dev", "This is the dev test account", "fb29bd1f92777108629d5ffcbadfa71206013587e483ac6d468fef5f4c845423b43f3f3726c3bc3a4eaaf300f1a6d8199257e3ef85b7164f195a3aa954ff4992");

insert into www.posts(author, content, title) values(1, "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque sollicitudin purus cursus nunc laoreet condimentum. Proin eget diam sit amet libero malesuada luctus sit amet nec magna. Sed convallis interdum aliquam. Interdum et malesuada fames ac ante ipsum primis in faucibus. Proin quis aliquam tortor. Morbi iaculis ut nibh ut congue. Mauris sed placerat dolor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sit amet rhoncus nisl. Sed augue sem, viverra ut laoreet id, ultricies non urna. In tristique aliquam dictum. Praesent convallis eget est consequat volutpat. Suspendisse non nulla ut dui tempus feugiat sed et leo. Suspendisse eu velit leo. Praesent tortor lacus, molestie eu eleifend eget, consectetur quis dolor. Praesent vel semper magna. Nulla maximus feugiat tincidunt. Donec nec est lobortis, hendrerit augue in, pharetra nisi. Proin ut scelerisque quam, ut euismod justo. Aenean iaculis pharetra ipsum, vulputate congue lorem blandit et. Cras maximus ante dolor. Sed tempor diam odio, eu scelerisque risus commodo nec. Sed consequat commodo quam eget feugiat. Praesent viverra, neque non euismod fringilla, ante sem facilisis tortor, eu posuere ipsum arcu pellentesque est. Proin rhoncus egestas augue quis mollis. Vivamus eu augue tempus, mattis massa et, sollicitudin mauris. Aenean lectus dolor, ornare id mollis ac, sagittis sit amet justo. Etiam a scelerisque eros. Curabitur eget ante nec neque elementum interdum interdum nec urna. Maecenas in velit ac felis molestie varius. Proin vitae ullamcorper sem, vitae elementum mauris. Etiam in felis varius, auctor lorem vitae, aliquam augue. Donec fermentum semper tempor. Mauris eget nunc iaculis, tempus dolor id, vehicula mauris. Mauris bibendum lorem justo, cursus dictum est iaculis a. Nam convallis, lorem lacinia tristique imperdiet, purus tortor auctor ex, sed blandit lacus lectus et magna. Mauris lacus orci, pretium quis turpis eget, viverra sagittis velit. Donec accumsan ultricies ullamcorper leo. ", "Lorem Ipsum");
    
insert into www.posts(author, title, content) values(1, "Markdown Test", LOAD_FILE('/var/www/htdocs/docs/test.md'));

insert into www.posts(author, title, content) values(1, "Dev Log", LOAD_FILE('/var/www/htdocs/docs/devlog.md'));

insert into www.comments(post, author, content) values(1, 1, "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus et nisi magna. Maecenas quis maximus justo. Proin lacinia orci ex, vel faucibus odio lacinia at. Vestibulum cursus erat sed enim venenatis, non porttitor tortor facilisis. Donec pharetra molestie risus id volutpat. Morbi vitae ex sagittis libero aliquam consectetur. Fusce id placerat neque. Nullam accumsan dui eu mi ultricies hendrerit. Praesent mauris orci, fermentum bibendum gravida sodales, dignissim id felis. Donec lectus eros.");

alter table www.posts add constraint check(length(content) >= 2000);
alter table www.comments add constraint check(length(content) >= 500);



GRANT ALL privileges ON www.* TO 'www'@'localhost';
FLUSH privileges;


